<?php

namespace tonisormisson\packageinfo\models;

use tonisormisson\packageinfo\PackageInfo;
use tonisormisson\packageinfo\services\ResponseService;

/**
 * Class ScrutinizerRepository
 * @property boolean $isPassed
 *
 * @author Tõnis Ormisson <tonis@andmemasin.eu>
 */
class Scrutinizer extends ApiClient
{
    /** @var Packagist */
    public $packagist;

    public $colors = [
        'veryGood' =>"#007E33",
        'good' =>"#00C851",
        'satisfactory' =>"#ffbb33",
        'pass' =>"#ff4444",
        'critical' =>"#CC0000",
    ];

    public $buildStatus = "failed";

    /** @var float  */
    public $quality = 0.0;
    /** @var float  */
    public $weightVeryGood = 0.0;
    /** @var float  */
    public $weightGood = 0.0;
    /** @var float  */
    public $weightSatisfactory = 0.0;
    /** @var float  */
    public $weightPass = 0.0;
    /** @var float  */
    public $weightCritical = 0.0;



    public function init()
    {
        parent::init();
        $this->populate();
    }

    public function populate(){
        $response = ResponseService::getResponse($this->getUrl());
        if (isset($response['applications']) && isset($response['applications'][$this->branch]["index"]["_embedded"]["project"]["metric_values"]["scrutinizer.quality"])) {
            $applications   = $response['applications'];
            $branch = $applications[$this->branch];
            $this->buildStatus = $branch["build_status"]["status"];

            $metrics = $branch["index"]["_embedded"]["project"]["metric_values"];
            $this->quality =$metrics["scrutinizer.quality"];
            $this->weightVeryGood =$metrics["scrutinizer.weight.very_good"];
            $this->weightGood =$metrics["scrutinizer.weight.good"];
            $this->weightSatisfactory =$metrics["scrutinizer.weight.satisfactory"];
            $this->weightPass =$metrics["scrutinizer.weight.pass"];
            $this->weightCritical =$metrics["scrutinizer.weight.critical"];
        }
    }

    public function getUrl() {
        return "https://scrutinizer-ci.com/api/repositories/{$this->packageType}/{$this->vendor}/{$this->package}?access_token={$this->accessKey}";
    }

    /**
     * @return bool
     */
    public function getIsPassed(){
        return ($this->buildStatus === "passed");
    }


}