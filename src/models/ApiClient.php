<?php
namespace tonisormisson\packageinfo\models;

use tonisormisson\packageinfo\PackageInfo;
use yii\base\Component;

/**
 * Class ApiClient
 * @property string url
 * @property string $vendor
 * @property string $package
 * @property string $loginName
 *
 * @package tonisormisson\packageinfo\models
 * @author Tõnis Ormisson <tonis@andmemasin.eu>
 */
abstract class ApiClient extends Component
{
    public $accessKey = '';
    public $packageName = '';
    public $branch = "master";
    public $packageType = PackageInfo::SOURCE_GITHUB;

    /** @var PackageInfo */
    public $packageInfo;

    /** @var string $loginName define this only if the vendor name & login name differ */
    public $loginName = null;

    public function init()
    {
        parent::init();
        $this->packageName = $this->packageInfo->packageName;
        $this->loginName = $this->packageInfo->loginName;
        $this->packageType = $this->packageInfo->packageType;
        $this->populate();
    }

    abstract function populate();

    /** @return string */
    abstract public function getUrl();

    /**
     * @return string
     */
    public function getVendor() {
        if(!empty($this->loginName)) {
            return $this->loginName;
        }
        $pieces = explode("/", $this->packageInfo->packageName);
        return $pieces[0];
    }

    /**
     * @return string
     */
    public function getPackage() {
        $pieces = explode("/", $this->packageInfo->packageName);
        return $pieces[1];
    }


}