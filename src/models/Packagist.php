<?php
namespace tonisormisson\packageinfo\models;

use tonisormisson\packageinfo\services\ResponseService;

/**
 * Class Packagist
 * @property $packageUrl
 *
 * @package tonisormisson\packageinfo\models
 * @author Tõnis Ormisson <tonis@andmemasin.eu>
 */
class Packagist extends ApiClient
{
    /** @var int  */
    public $totalDownloads = 0;

    public function init()
    {
        parent::init();
        $this->loginName = null;
        $this->populate();

    }

    /** @var string $repository repository url */
    public $repository;

    public function populate()
    {
        $response = ResponseService::getResponse($this->getUrl());
        if (is_array($response) && isset($response["package"])) {
            $data = $response["package"]["downloads"];
            $this->totalDownloads = $data["total"];
            $this->repository = $response["package"]["repository"];
        }
    }

    /** @return string */
    public function getUrl()
    {
        return $this->packageUrl . ".json";
    }

    /** @return string */
    public function getPackageUrl()
    {
        return "https://packagist.org/packages/{$this->vendor}/{$this->package}";
    }


}