<?php
namespace tonisormisson\packageinfo\models;

use tonisormisson\packageinfo\PackageInfo;
use yii\base\Component;

/**
 * Class Codecov
 * @property string $badgeUrl
 * @package tonisormisson\packageinfo\models
 * @author Tõnis Ormisson <tonis@andmemasin.eu>
 */
class Codecov extends ApiClient
{
    public $enabled = false;

    public function getUrl() {
        return "https://codecov.io/" . $this->type() ."/".  $this->vendor  .
            "/" . $this->packageInfo->packagist->package;
    }

    private function type()
    {
        if ($this->packageInfo->packageType === PackageInfo::SOURCE_BITBUCKET) {
            return "bb";
        }
        return "gh";
    }
    public function getBadgeUrl() {
        return $this->url . "/branch/" . $this->branch . "/graph/badge.svg";
    }

    public function populate()
    {
        $this->enabled = $this->packageInfo->codecovEnabled;
    }
}