<?php

use yii\web\View;
use dosamigos\chartjs\ChartJs;

/* @var \tonisormisson\packageinfo\models\Scrutinizer $model */
/* @var View $this */

$formatter = new \yii\i18n\Formatter();
?>


<?= ChartJs::widget([
    'type' => 'doughnut',
    'options' => [
        'width' => 4,
    ],
    'clientOptions' => [
        'cutoutPercentage' => 70,
        'legend' => ['display' => false],
        'elements' => [
            "center" => [
                "text" => $formatter->asDecimal($model->quality,1),
            ],
        ],
    ],
    'data' => [
        'legend' => [
            'position' => "left",
            'display' => false,
        ],
        'labels' => ["Very Good", "Good", "Satisfactory", "Pass", "Critical"],
        'datasets' => [
            [
                "borderWidth" =>  0,
                'label' => "My First dataset",
                'backgroundColor' => [$model->colors['veryGood'], $model->colors['good'], $model->colors['satisfactory'], $model->colors['pass'], $model->colors['critical']],
                'data' => [$model->weightVeryGood, $model->weightGood, $model->weightSatisfactory, $model->weightPass, $model->weightCritical]
            ],
        ]
    ],
    'plugins' =>
        new \yii\web\JsExpression("
        [{
             beforeDraw: function (chart) {
                if (chart.config.options.elements.center) {
                  //Get ctx from string
                  var ctx = chart.chart.ctx;
            
                  //Get options from the center object in options
                  var centerConfig = chart.config.options.elements.center;
                  var fontStyle = centerConfig.fontStyle || 'Arial';
                  var txt = centerConfig.text;
                  var color = centerConfig.color || '#000';
                  var sidePadding = centerConfig.sidePadding || 20;
                  var sidePaddingCalculated = (sidePadding/100) * (chart.innerRadius * 2)
                  //Start with a base font of 30px
                  ctx.font = \"30px \" + fontStyle;
            
                  //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
                  var stringWidth = ctx.measureText(txt).width;
                  var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;
            
                  // Find out how much the font can grow in width.
                  var widthRatio = elementWidth / stringWidth;
                  var newFontSize = Math.floor(30 * widthRatio);
                  var elementHeight = (chart.innerRadius * 2);
            
                  // Pick a new font size so it will not be larger than the height of label.
                  var fontSizeToUse = Math.min(newFontSize, elementHeight);
            
                  //Set font settings to draw it correctly.
                  ctx.textAlign = 'center';
                  ctx.textBaseline = 'middle';
                  var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
                  var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
                  ctx.font = fontSizeToUse+\"px \" + fontStyle;
                  ctx.fillStyle = color;
            
                  //Draw text in center
                  ctx.fillText(txt, centerX, centerY);
                }
              }            
        }]")
]);
?>
