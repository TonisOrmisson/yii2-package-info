<?php

use yii\helpers\Html;
use yii\web\View;
use kartik\icons\Icon;


/* @var \tonisormisson\packageinfo\PackageInfo $widget */
/* @var View $this */

\dmstr\web\AdminLteAsset::register($this);
$formatter = new \yii\i18n\Formatter();
?>

<div class="box box-<?=$widget->status?>">
    <div class="box-header with-border">
        <?= Html::tag("div", Html::encode($widget->packageName), []) ?>
    </div>
    <div class="box-body text-center">
        <?= $this->render("_chart", ["model" => $widget->scrutinizer])?>
        <div class="text-uppercase h4 margin-none padding-none">
            <?= Html::tag("div", Html::encode($widget->scrutinizer->buildStatus), ['class' => "label label-{$widget->status}"])?>
        </div>
        <?= ($widget->codecov->enabled ? Html::a(Html::tag("div", Html::img($widget->codecov->badgeUrl)), $widget->codecov->url, ["target" => "_blank"]) : null)?>
    </div>
    <div class="box-footer">
        <?= Html::a(Icon::show('github-alt', [], Icon::FA), Html::encode($widget->packagist->repository), ["target" => "_blank"]); ?>&nbsp;
        <?= Html::a(Icon::show('gift', [], Icon::FA), Html::encode($widget->packagist->packageUrl), ["target" => "_blank"]); ?>&nbsp;
        <?= Html::tag("div", Html::tag("span",null, ['class' => "glyphicon glyphicon-download"]) . " " . $formatter->asInteger($widget->packagist->totalDownloads), $widget->footerOptions)?>
    </div>
</div>