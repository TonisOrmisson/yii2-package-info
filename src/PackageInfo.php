<?php
namespace tonisormisson\packageinfo;

use tonisormisson\packageinfo\models\Codecov;
use tonisormisson\packageinfo\models\Packagist;
use tonisormisson\packageinfo\models\Scrutinizer;
use yii\base\Widget;
use yii\i18n\PhpMessageSource;

/**
 * Class JsonForm
 * @property boolean $isHorizontal
 * @property Scrutinizer $scrutinizer
 * @property Packagist $packagist
 * @property Codecov $codecov
 * @property string $status a bootstrab status eg "success" | "danger" etc
 *
 * @package tonisormisson\jsonform
 * @author Tõnis Ormisson <tonis@andmemasin.eu>
 */
class PackageInfo extends Widget
{
    const SOURCE_GITHUB = "g";
    const SOURCE_BITBUCKET = "b";

    /** @var string $loginName define this only if the vendor name & login name differ */
    public $loginName = null;

    public $packageName = "vendor/package";
    public $scrutinizerToken = "my-access-token";
    public $packageType = PackageInfo::SOURCE_GITHUB;
    public $codecovEnabled = false;

    /** @var Scrutinizer */
    private $_scrutinizer;

    /** @var Packagist */
    private $_packagist;

    /** @var Codecov */
    private $_codecov;

    /** @var string */
    private $_status;

    /** @var array $footerOptions HTML options for footer */
    public $footerOptions = [];

    public function init()
    {
        parent::init();
        $this->registerTranslations();

        if (empty($this->footerOptions)) {
            $this->footerOptions = [
                'class' => 'pull-right',
                'data-toggle' => "tooltip",
                'title' => \Yii::t("topackage", "Total downloads"),
            ];
        }
    }

    public function registerTranslations()
    {
        \Yii::$app->i18n->translations['topackage'] = [
            'class' => PhpMessageSource::class,
            'sourceLanguage' => 'en-US',
            'basePath' => __DIR__ . '/../messages',
        ];
    }

    public function run()
    {
        $this->loadPackagist();
        $this->loadScrutinizer();
        $this->loadCodecov();
        $this->loadStatus();
        return $this->render('index', [
            'widget' => $this
        ]);
    }

    private function  loadStatus(){
        if(!$this->scrutinizer->isPassed) {
            $this->_status = "danger";
            return null;
        }
        if ($this->scrutinizer->quality >= 8) {
            $this->_status = "success";
            return null;
        }
        if ($this->scrutinizer->quality >= 6) {
            $this->_status = "warning";
            return null;
        }
        $this->_status = "danger";
        return null;

    }

    private function loadScrutinizer()
    {
        $this->_scrutinizer = new Scrutinizer([
            'accessKey' => $this->scrutinizerToken,
            'packagist' => $this->packagist,
            'packageInfo' => &$this,
        ]);
    }

    private function loadPackagist()
    {
        $this->_packagist = new Packagist([
            'packageInfo' => &$this,
        ]);
    }

    private function loadCodecov()
    {
        $this->_codecov = new Codecov([
            'packageInfo' => &$this,
        ]);
    }

    /**
     * @return Scrutinizer
     */
    public function getScrutinizer(): Scrutinizer
    {
        return $this->_scrutinizer;
    }

    /**
     * @return Packagist
     */
    public function getPackagist(): Packagist
    {
        return $this->_packagist;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->_status;
    }

    /**
     * @return string
     */
    public function getCodecov(): Codecov
    {
        return $this->_codecov;
    }
}