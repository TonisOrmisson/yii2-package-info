<?php

namespace tonisormisson\packageinfo\services;

use yii\helpers\Json;

class ResponseService
{

    /**
     * @param string $url
     * @param bool $decode whether we need to decode json
     * @return mixed
     */
    public static function getResponse($url)
    {
        $agent= 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';

        $options = array(
            CURLOPT_RETURNTRANSFER => true,   // return web page
            CURLOPT_HEADER         => false,  // don't return headers
            CURLOPT_FOLLOWLOCATION => true,   // follow redirects
            CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
            CURLOPT_ENCODING       => "",     // handle compressed
            CURLOPT_USERAGENT      => "test", // name of client
            CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 5,      // time-out on connect
            CURLOPT_TIMEOUT        => 5,      // time-out on response
            CURLOPT_USERAGENT => $agent
        );

        $ch = curl_init($url);
        curl_setopt_array($ch, $options);
        $content  = curl_exec($ch);
        if (!$content){
            \Yii::error(curl_error($ch),__METHOD__);
            return false;
        }
        curl_close($ch);

        return json_decode($content, true);
    }

}